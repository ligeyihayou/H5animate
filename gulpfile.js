//引入插件
var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-minify-css');
var clean = require('gulp-clean');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var gulpSequence = require('gulp-sequence');
var concat = require('gulp-concat');                            //- 多个文件合并为一个；
var minifyCss = require('gulp-minify-css');                     //- 压缩CSS为一行；
var rev = require('gulp-rev')
var uglify = require('gulp-uglify')
var revCollector = require('gulp-rev-collector');

var option = {
    css: ['./css/animate.min.css', './css/page.css', './css/reset_ph_v=2016.css', './css/swiper.min.css'],
    js:['./jquery.min.js', './Screen.js','./swiper.animate.min.js','./swiper.min.js','./jquery.imgpreload.min.js','./swiper.animate.min.js','./page.js']
};

// 添加前缀
gulp.task('autoprefixer', function () {
    //找到src目录下app.css，为其补全浏览器兼容的css
    return gulp.src('css/*.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        //输出到dist文件夹
        .pipe(gulp.dest('dist/css'));
});

// 压缩css
gulp.task('testCssmin', function () {
    gulp.src('css/*.css')
        .pipe(cssmin())
        .pipe(gulp.dest('dist/css'));
});
// css合并
gulp.task('concat', function () {
    gulp.src(option.css)
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(concat('wap.min.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest('./dist/css'))

});

// js 压缩
gulp.task('script', function () {
    gulp.src('js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
})

// 图片压缩
gulp.task('imagemin', function () {
    return gulp.src('images/**')
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dist/images'));
});
// 辅助文件
gulp.task('copy', function () {
    return gulp.src('index.html')
        .pipe(gulp.dest('dist'))
});
// 清除文件
gulp.task('clean', function () {
    return gulp.src(['dist', 'rev'], {read: true})
        .pipe(clean());
});


//默认执行任务
// gulp.task('default', gulpSequence('autoprefixer', function () {
//     console.log("输出完成！");
// }));

