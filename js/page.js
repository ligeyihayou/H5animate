/**
 * Created by zyn on 04/11/2017.
 */
$(function () {
    var images = [];
    var imgNum = 0;
    var audio = document.getElementById('audio');
    var isdebug = false;
    var ServerPath = "http://h5.zhibaba.com/index/";
    //var ServerPath = "http://sharev.heysound.com:9002/";
    // 预先加载图片地址 该项目用到的图片绝对地址
    if (!isdebug) {
        images.push(ServerPath + "images/loading-icon/loading.png");
        images.push(ServerPath + "images/loading-icon/loading-icon1.png");
        images.push(ServerPath + "images/loading-icon/loading-icon2.png");
        images.push(ServerPath + "images/loading-icon/loading-icon3.png");
        images.push(ServerPath + "images/loading-icon/loading-icon4.png");
        images.push(ServerPath + "images/loading-icon/loading-icon5.png");
        images.push(ServerPath + "images/loading-icon/loading-icon6.png");
        images.push(ServerPath + "images/loading-icon/loading-icon7.png");
        images.push(ServerPath + "images/loading-icon/loading-pic1.png");
        images.push(ServerPath + "images/page1/icon_arrow_1.png");
        images.push(ServerPath + "images/page1/silder-2.png");
        images.push(ServerPath + "images/page1/silder-2-icon1.png");
        images.push(ServerPath + "images/page1/solder-2-icon2.png");
        images.push(ServerPath + "images/page1/solder-2-icon3.png");
        images.push(ServerPath + "images/page2/page2-bird.png");
        images.push(ServerPath + "images/page2/page2-border.png");
        images.push(ServerPath + "images/page2/page2-bottom-left-icon.png");
        images.push(ServerPath + "images/page2/page2-bottom-right-icon.png");
        images.push(ServerPath + "images/page2/page2-left-top-icon.png");
        images.push(ServerPath + "images/page2/page2-logo.png");
        images.push(ServerPath + "images/page2/page2-right-top-icon.png");
        images.push(ServerPath + "images/page3/font-1.png");
        images.push(ServerPath + "images/page3/font-2.png");
        images.push(ServerPath + "images/page3/font-3.png");
        images.push(ServerPath + "images/page3/font-4.png");
        images.push(ServerPath + "images/page3/font-5.png");
        images.push(ServerPath + "images/page3/font-6.png");
        images.push(ServerPath + "images/page3/page3-bg.png");
        images.push(ServerPath + "images/page3/page3-line.png");
        images.push(ServerPath + "images/page3/page3-logo.png");
        images.push(ServerPath + "images/page4/address-icon1.png");
        images.push(ServerPath + "images/page4/address-icon2.png");
        images.push(ServerPath + "images/page4/map.png");
        images.push(ServerPath + "images/page4/page4.png");
        images.push(ServerPath + "images/page5/page5-bg.png");
        images.push(ServerPath + "images/page5/page5-font.png");
        images.push(ServerPath + "images/page5/page5-icon1.png");
        images.push(ServerPath + "images/page5/page5-logo.png");
        images.push(ServerPath + "images/page5/pageWX-icon.png");
        images.push(ServerPath + "images/icon_music_on.png");
        images.push(ServerPath + "images/font/font1.png");
        images.push(ServerPath + "images/font/font2.png");
        images.push(ServerPath + "images/font/font3.png");
        images.push(ServerPath + "images/font/font4.png");
        images.push(ServerPath + "images/font/font5.png");
        images.push(ServerPath + "images/font/font6.png");
        images.push(ServerPath + "images/font/font7.png");
        images.push(ServerPath + "images/font/font8.png");
        images.push(ServerPath + "images/font/font9.png");
        images.push(ServerPath + "images/font/font10.png");
    } else {
        images.push("https://assets-cdn.github.com/images/modules/site/site-signup-prompt.png");
        images.push("http://a.hiphotos.baidu.com/image/pic/item/a686c9177f3e670909f483ec31c79f3df8dc5529.jpg");
        images.push("http://f.hiphotos.baidu.com/image/pic/item/060828381f30e924f5bf6d2246086e061d95f78e.jpg");
        images.push("http://e.hiphotos.baidu.com/image/pic/item/838ba61ea8d3fd1ff54162293a4e251f95ca5f5d.jpg");
    }
    var options = {
        on: {
            init: function () {
                swiperAnimateCache(this); //隐藏动画元素
                swiperAnimate(this); //初始化完成开始动画
            },
            slideChange: function () {
                swiperAnimate(this); //每个slide切换结束时也运行当前slide动画
            }
        },
        slidesPerView: 1,
        spaceBetween: 0,
        direction: 'vertical',
        mousewheelControl: true
    };
    var Page = {
        init: function () {
            this.imgload();
        },
        swiper: function () {
            new Swiper('.swiper-container', options);
        },
        imgload: function () {
            $.imgpreload(images, {
                each: function () {
                    /*this will be called after each image loaded*/
                    var status = $(this).data('loaded') ? 'success' : 'error';
                    if (status == "success") {
                        var v = (parseFloat(++imgNum) / images.length).toFixed(2);
                        $(".loading-progress p").html(Math.round(v * 100) + "%");
                        $(".loading-progress span").width(Math.round(v * 100) + "%");
                    }
                },
                all: function () {
                    $(".loading-progress span").width("100%");
                    $(".loading-progress p").html("100%");
                    $(".loading-Conatiner").fadeOut(100, function () {
                        Page.swiper();
                        Page.music();
                    });
                }
            });
        },
        music: function () {
            $('#MusicHang').on('click', function () {
                var index = $("#Music").attr('data-play');
                if (index === 'ok') {
                    audio.pause();
                    $("#Music").removeClass('music-play');
                    $("#Music").attr('data-play', 'on');
                } else {
                    audio.play();
                    $("#Music").addClass('music-play');
                    $("#Music").attr('data-play', 'ok');
                }
            });
        }
    };
    Page.init();
});